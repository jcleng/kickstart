# ubuntu-20.04.1-legacy-server Kickstart

参考[https://github.com/vrillusions/ubuntu-kickstart/blob/master/20.04/README.md](https://github.com/vrillusions/ubuntu-kickstart/blob/master/20.04/README.md)

[ubuntu kickstart](https://doc.ubuntu-fr.org/kickstart)

镜像需要ubuntu-legacy-server, 使用qemu启动的时候cpu请使用1或者不指定

编辑 `/isolinux/txt.cfg` 和 `boot/grub/grub.cfg`, windows用软碟通, linux用xorriso命令

[配置文档](https://pykickstart.readthedocs.io)

[配置文档2](https://docs.fedoraproject.org/en-US/fedora/f35/install-guide/appendixes/Kickstart_Syntax_Reference/#appe-kickstart-syntax-reference)

```cfg
# ubuntu-20.04.1-legacy-server-amd64.iso 配置使用, 编辑启动项增加ks参数, ks的地址需要网络地址

append  file=/cdrom/preseed/ubuntu-server.seed vga=788 initrd=/install/initrd.gz ks=https://jihulab.com/jcleng/kickstart/-/raw/main/20.04/ks-2004-minimalvm.cfg quiet ---

# boot/grub/grub.cfg 文件一样的
menuentry "Install Ubuntu Server" {
	set gfxpayload=keep
	linux	/install/vmlinuz  file=/cdrom/preseed/ubuntu-server.seed ks=https://jihulab.com/jcleng/kickstart/-/raw/main/20.04/ks-2004-minimalvm.cfg quiet ---
	initrd	/install/initrd.gz
}
```

- linux上操作

```shell
xorriso -indev ubuntu-20.04.6-live-server-amd64.iso -outdev out.iso -boot_image isolinux dir=/isolinux -add isolinux
file out.iso

tree
# .
# ├── disk-virt.img
# ├── isolinux
# │   └── txt.cfg
# ├── out.iso
# ├── run.sh
# ├── txt.cfg
# └── ubuntu-20.04.1-legacy-server-amd64.iso

# 启动安装之后 ctrl+alt+f4 可以显示日志
```
